#line 1 "device/qcom/sepolicy/generic/private/property_contexts"
# Copyright (c) 2017, 2019, 2021 The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

ro.vendor.qti.va_aosp.support       u:object_r:vendor_exported_system_prop:s0 exact bool
ro.vendor.qti.va_odm.support       u:object_r:vendor_exported_odm_prop:s0 exact bool
ro.vendor.perf.scroll_opt        u:object_r:vendor_exported_system_prop:s0 exact bool
ro.vendor.perf.scroll_opt.heavy_app        u:object_r:vendor_exported_system_prop:s0 exact int
ro.netflix.bsp_rev                         u:object_r:vendor_exported_system_prop:s0 exact string

persist.vendor.dpm.                        u:object_r:vendor_persist_dpm_prop:s0
persist.vendor.rcs.                        u:object_r:vendor_persist_rcs_prop:s0
persist.vendor.tcmd.                       u:object_r:vendor_persist_tcm_prop:s0
persist.vendor.btstack                     u:object_r:bluetooth_prop:s0
persist.vendor.bluetooth.emailaccountcount u:object_r:bluetooth_prop:s0
persist.vendor.bt.a2dp                     u:object_r:bluetooth_prop:s0
persist.vendor.bt_logger.                  u:object_r:bluetooth_prop:s0
persist.vendor.service.bt.                 u:object_r:bluetooth_prop:s0
ro.vendor.btstack.                         u:object_r:bluetooth_prop:s0
vendor.pts.                                u:object_r:bluetooth_prop:s0
vendor.bt.pts.                             u:object_r:bluetooth_prop:s0
vendor.bluetooth.                          u:object_r:bluetooth_prop:s0
vendor.camera.aux.packagelist              u:object_r:vendor_persist_camera_prop:s0
persist.vendor.camera.privapp.list         u:object_r:vendor_persist_camera_prop:s0

#mm-parser
vendor.mm.enable.qcom_parser       u:object_r:vendor_mm_parser_prop:s0
vendor.qcom_parser.                u:object_r:vendor_mm_parser_prop:s0
#mm-osal
vendor.debug.mmosal.config         u:object_r:vendor_mm_osal_prop:s0

#perf
vendor.perf.workloadclassifier.enable      u:object_r:vendor_wlc_prop:s0
persist.vendor.build.date.utc              u:object_r:vendor_wlc_prop:s0
vendor.mpctl.init.complete                 u:object_r:vendor_wlc_public_prop:s0
vendor.perf.framepacing.                   u:object_r:vendor_afp_prop:s0

#mm-video
persist.vendor.debug.av.logs.lvl          u:object_r:debug_prop:s0
persist.vendor.debug.en.drpcrpt           u:object_r:vendor_mm_video_prop:s0
persist.vendor.media.hls.                 u:object_r:vendor_mm_video_prop:s0
persist.vendor.sys.media.rtp-ports        u:object_r:vendor_mm_video_prop:s0
vendor.encoder.video.profile              u:object_r:vendor_mm_video_prop:s0
vendor.sys.media.target.version           u:object_r:vendor_sys_video_prop:s0
vendor.sys.video.disable.ubwc             u:object_r:vendor_sys_video_prop:s0
vendor.sys.media.target.qssi              u:object_r:vendor_sys_video_prop:s0

#Wifi Display
vendor.wfdservice                         u:object_r:vendor_wfd_service_prop:s0
persist.vendor.debug.wfd.wfdsvc           u:object_r:vendor_wfd_sys_debug_prop:s0
persist.vendor.debug.wfdcdbg              u:object_r:vendor_wfd_sys_debug_prop:s0
persist.vendor.debug.wfdcdbgv             u:object_r:vendor_wfd_sys_debug_prop:s0
persist.vendor.sys.debug.mux.             u:object_r:vendor_wfd_sys_debug_prop:s0
persist.vendor.sys.debug.rtp.             u:object_r:vendor_wfd_sys_debug_prop:s0
persist.vendor.sys.debug.wfd.             u:object_r:vendor_wfd_sys_debug_prop:s0
vendor.sys.debug.wfd.                     u:object_r:vendor_wfd_sys_debug_prop:s0
persist.vendor.setWFDInfo.R2              u:object_r:vendor_wfd_sys_prop:s0

# WIGIG
persist.vendor.wigig.                      u:object_r:vendor_wigig_core_prop:s0
persist.vendor.fst.                        u:object_r:vendor_fst_prop:s0
persist.dpm.feature                        u:object_r:vendor_persist_dpm_prop:s0
ctl.stop$dpmd                              u:object_r:ctl_dpmd_prop:s0
ctl.stop$tcmd                              u:object_r:ctl_tcmd_prop:s0

# Beluga
ro.vendor.beluga.p                         u:object_r:vendor_exported_system_prop:s0
ro.vendor.beluga.c                         u:object_r:vendor_exported_system_prop:s0
ro.vendor.beluga.s                         u:object_r:vendor_exported_system_prop:s0
ro.vendor.beluga.t                         u:object_r:vendor_exported_system_prop:s0

#XRCB prop
vendor.xrcb.                               u:object_r:vendor_xrcb_prop:s0
vendor.sxrauxd                             u:object_r:vendor_xrcb_prop:s0

# bootreceiver config props
ro.vendor.bootreceiver.enable              u:object_r:vendor_bootreceiver_prop:s0 exact bool

#Power Module
ro.vendor.power.tuning.support             u:object_r:vendor_exported_system_prop:s0 exact bool
#line 1 "out/soong/.intermediates/system/sepolicy/contexts/system_ext_property_contexts/android_common/gen/newline"

#line 1 "vendor/honor/common/sepolicy/base/system_ext/private/property_contexts"
# add for hwouc to set patch version
mscw.hnouc.            u:object_r:hnouc_prop:s0
instantshare.sending   u:object_r:instantshare_prop:s0
persist.deep.theme      u:object_r:thememanager_deep_prop:s0

persist.hw.power.shutdown       u:object_r:bootanim_prop:s0
persist.dbg.volte_avail_ovr u:object_r:ims_debug_prop:s0

persist.hiview.logblacklist   u:object_r:log_tag_prop:s0

xcollie.s.              u:object_r:xcollie_prop:s0

vold.cryptsd.state       u:object_r:vold_prop:s0
vold.cryptsd_progress       u:object_r:vold_prop:s0
vold.cryptsd.keystate       u:object_r:vold_prop:s0

sys.tpm_start   u:object_r:tpm_start_prop:s0

#for recovery
recovery.hw_resize_userdata_flg     u:object_r:recovery_prop:s0
factory_reset.stop_srv              u:object_r:recovery_prop:s0
factory.recovery.prepare            u:object_r:recovery_prop:s0
modem_update                        u:object_r:recovery_prop:s0
ro.modem_update                     u:object_r:recovery_prop:s0
odm.sys.remove_dm                   u:object_r:recovery_prop:s0
odm.sys.make_update_dir             u:object_r:recovery_make_update_dir_prop:s0

dev.fty_rst_firstboot                    u:object_r:fac_reboot_flag_prop:s0
persist.cust_service.done           u:object_r:cust_prop:s0
update.auth.prop.pass               u:object_r:auth_pass_prop:s0
#for sar
reduce.sar.imsi.mnc		u:object_r:radio_prop:s0

#for hwnff
hwnff.server.start		u:object_r:log_tag_prop:s0

cota.update.opkey.version.enable  u:object_r:cota_prop:s0
cota.update.update_status_report u:object_r:cota_prop:s0
cota.live_update.handle_version_img u:object_r:cota_prop:s0
cota.live_update.set_cust_policy_dirs u:object_r:cota_prop:s0
cota.live_update.cota_vendor_country u:object_r:cota_prop:s0

persist.sys.hiview.debug        u:object_r:hiviewdebug_prop:s0
persist.sys.usb.kidsmode        u:object_r:usb_control_prop:s0 exact string

ctl.xlogctl_service     u:object_r:dftlogcat_prop:s0
ctl.xlogcat_service     u:object_r:dftlogcat_prop:s0
ctl.xlogview_service        u:object_r:dftlogcat_prop:s0
ctl.chargelogcat        u:object_r:dftlogcat_prop:s0
ctl.kmsglogcat          u:object_r:dftlogcat_prop:s0
ctl.applogcat           u:object_r:dftlogcat_prop:s0
ctl.sleeplogcat         u:object_r:dftlogcat_prop:s0
ctl.rillogcat           u:object_r:dftlogcat_prop:s0
ctl.kmsgcat_cp          u:object_r:dftlogcat_prop:s0
ctl.isplogcat           u:object_r:dftlogcat_prop:s0
ctl.inputlogcat         u:object_r:dftlogcat_prop:s0
dumptool.               u:object_r:dftlogcat_prop:s0
ctl.faclog_service      u:object_r:dftlogcat_prop:s0

persist.sys.smart_power     u:object_r:powergenie_prop:s0
hw.flash.disabled.by.low_temp   u:object_r:powergenie_prop:s0
sys.thermal.camera_esc_level   u:object_r:powergenie_prop:s0
sys.thermal.media_esc_level   u:object_r:powergenie_prop:s0

odm.drm.stop  u:object_r:system_prop:s0
persist.odm.restart.mediadrm  u:object_r:system_prop:s0
persist.mdm.animation  u:object_r:animation_prop:s0

runtime.mmitest.isrunning u:object_r:mmi_prop:s0

runtime.nfc.test u:object_r:mmi_prop:s0

# for init
mscw.hnouc.cota.prop.liveupdate u:object_r:cota_liveupdate_prop:s0
mscw.hnouc.update.distribute.init u:object_r:cota_liveupdate_prop:s0
# for goldeneye
goldeneye.	u:object_r:goldeneye_prop:s0

#gnss
vendor.gps_daemon_reload u:object_r:system_prop:s0
ro.odm.config.gnss_adapt_card u:object_r:system_prop:s0

#cameraserver
config.camera.color_encode u:object_r:camera_color_encode_prop:s0

# for AGP Service
agp.version u:object_r:agp_version_prop:s0
magic_agp_ic_adaptive_fps_show_enable      u:object_r:magic_agp_ic_adaptive_fps_show_enable_prop:s0
magic.config.support_low_framerate     u:object_r:agp_support_low_framerate_prop:s0
sys.view.promotion.switch   u:object_r:agp_view_promotion_enable_prop:s0

# nfc
ro.product.cuptsm    u:object_r:product_cuptsm_prop:s0
ro.config.nfc_nxp_active u:object_r:config_nfc_nxp_active_prop:s0
ro.vendor.connectivity.chiptype u:object_r:vendor_connectivity_prop:s0

#for Opsl LL
persist.config.lower_buffer_num_enable u:object_r:audio_prop:s0
persist.config.pcm_data_size u:object_r:audio_prop:s0

#for app scene detecting
bt.scene.app_type u:object_r:audio_prop:s0

persist.mygote.escape.enable  u:object_r:mygote_prop:s0
persist.mygote.disable  u:object_r:mygote_prop:s0
persist.mygote.ss.disable  u:object_r:mygote_prop:s0

#for watch
persist.odm.pairied.completed  u:object_r:pairied_prop:s0

# for apex
apexd.hep.install.status u:object_r:apexd_prop:s0 exact enum starting ready

#for hilog.debug
hilog.flowctrl.               u:object_r:log_tag_prop:s0
persist.lastusertype          u:object_r:log_tag_prop:s0

#for nearby
persist.sys.nearby.uuid u:object_r:nearby_uuid_prop:s0 exact string
msc.nearby.inited      u:object_r:nearby_prop:s0 exact string

#for one-time permission
msc.permission.support_onetime_allow u:object_r:one_time_permission_prop:s0

#for set cust
odm.set_cust_info u:object_r:cust_prop:s0

#for set cust commercial_preload
odm.set_cust_commercial_preload u:object_r:cust_prop:s0

#for delete actual_preload_list
odm.delete_actual_preload_list u:object_r:cota_prop:s0

#for set region cust
odm.set_region_cust_info u:object_r:cust_prop:s0

#for set cota info vc
odm.set_cota_info_vc u:object_r:cota_prop:s0

#for update custom cota vc
cota.update.update_custom_cota_vc.enable  u:object_r:cota_prop:s0

#for symlink data cust
cota.update.symlink_data_cust.enable  u:object_r:cota_prop:s0
# each prop in whitelist should belong to one owner, and list the owner in comment
# vendor-init-readable


# vendor-init-settable


# vendor-init-readable|vendor-init-actionable


# vendor-init-settable|vendor-init-actionable


# public-readable
ro.magic.systemversion u:object_r:honor_version_public_read_prop:s0 exact string
ro.comp.system_version u:object_r:honor_version_public_read_prop:s0 exact string
ro.confg.hw_systemversion u:object_r:honor_version_public_read_prop:s0 exact string
ro.comp.hl.product_base_version u:object_r:honor_version_public_read_prop:s0 exact string
ro.comp.hl.product_cust_version u:object_r:honor_version_public_read_prop:s0 exact string
ro.comp.hl.product_preload_version u:object_r:honor_version_public_read_prop:s0 exact string
ro.comp.hl.product_base_version.real u:object_r:honor_version_public_read_prop:s0 exact string
ro.comp.hl.product_cust_version.real u:object_r:honor_version_public_read_prop:s0 exact string
ro.comp.hl.product_preload_version.real u:object_r:honor_version_public_read_prop:s0 exact string
ro.comp.preas_version u:object_r:honor_version_public_read_prop:s0 exact string
ro.comp.preavs_version u:object_r:honor_version_public_read_prop:s0 exact string
ro.comp.prets_version u:object_r:honor_version_public_read_prop:s0 exact string
ro.comp.pretvs_version u:object_r:honor_version_public_read_prop:s0 exact string
msc.product.CotaVersion u:object_r:honor_version_public_read_prop:s0 exact string
msc.dg.img u:object_r:honor_version_public_read_prop:s0 exact string
msc.dg.img.resume u:object_r:honor_version_public_read_prop:s0 exact string
ro.comp.product_version u:object_r:honor_version_public_read_prop:s0 exact string
ro.comp.cust_version u:object_r:honor_version_public_read_prop:s0 exact string
ro.comp.version_version u:object_r:honor_version_public_read_prop:s0 exact string
ro.magic.userdataversion u:object_r:honor_version_public_read_prop:s0 exact string
ro.magic.recoveryversion u:object_r:honor_version_public_read_prop:s0 exact string
msc.sys.hota.is_hninit_exists u:object_r:honor_version_public_read_prop:s0 exact string
msc.sys.hota.is_hninit_cust_exists u:object_r:honor_version_public_read_prop:s0 exact string
msc.sys.hota.is_hninit_preload_exists u:object_r:honor_version_public_read_prop:s0 exact string
ro.magic.base_userdataversion u:object_r:honor_version_public_read_prop:s0 exact string
ro.magic.version_userdataversion u:object_r:honor_version_public_read_prop:s0 exact string
ro.magic.preload_userdataversion u:object_r:honor_version_public_read_prop:s0 exact string
ro.runmode u:object_r:honor_runmode_public_read_prop:s0 exact string
ro.image u:object_r:honor_boottype_public_read_prop:s0 exact string
ro.boot.oemmode u:object_r:honor_oemmode_public_read_prop:s0 exact string
ro.userlock u:object_r:honor_userlock_public_read_prop:s0 exact string
ro.build.hide u:object_r:honor_build_hide_prop:s0 exact string
ro.build.hide.matchers u:object_r:honor_build_hide_prop:s0 exact string
ro.build.hide.replacements u:object_r:honor_build_hide_prop:s0 exact string
ro.build.hide.settings u:object_r:honor_build_hide_prop:s0 exact string
ro.fastboot.unlock u:object_r:honor_fastboot_unlock_prop:s0 exact string
ro.mspes.config u:object_r:honor_config_mspes_prop:s0 exact string
ro.config.marketing_name u:object_r:honor_marketing_name_prop:s0 exact string
msc.config.use_security_patch u:object_r:honor_version_public_read_prop:s0 exact string
ro.build.ver.physical u:object_r:honor_version_public_read_prop:s0 exact string
ro.efusestate u:object_r:honor_efuse_state_prop:s0 exact string
ro.build.magic_api_level u:object_r:honor_version_public_read_prop:s0 exact string

# build props
ro.honor.build.display.id u:object_r:honor_version_public_read_prop:s0 exact string
ro.honor.build.fingerprint u:object_r:honor_version_public_read_prop:s0 exact string
ro.honor.build.host u:object_r:honor_version_public_read_prop:s0 exact string
ro.honor.build.version.incremental u:object_r:honor_version_public_read_prop:s0 exact string
ro.honor.build.version.security_patch u:object_r:honor_version_public_read_prop:s0 exact string
ro.honor.build.date.utc u:object_r:honor_version_public_read_prop:s0 exact string

# vendor-init-settable|public-readable
ro.magic.odmversion u:object_r:vendor_default_prop:s0 exact string
ro.magic.fastbootversion u:object_r:vendor_default_prop:s0 exact string
ro.magic.bootversion u:object_r:vendor_default_prop:s0 exact string
ro.comp.chipset_version u:object_r:vendor_default_prop:s0 exact string
ro.config.fast_switch_simslot u:object_r:radio_rild_public_prop:s0 exact string
msc.config.data_retry_enabled u:object_r:radio_rild_public_prop:s0 exact string
ro.config.full_network_support u:object_r:radio_rild_public_prop:s0 exact string
ro.config.dsds_mode u:object_r:radio_rild_public_prop:s0 exact string
msc.config.gcf u:object_r:radio_rild_public_prop:s0 exact string
msc.config.volte_on u:object_r:radio_rild_public_prop:s0 exact string
ro.lte.poorstd u:object_r:radio_rild_public_prop:s0 exact string
ro.radio.vsim_support u:object_r:radio_rild_public_prop:s0 exact string
ro.wcdma.poorstd u:object_r:radio_rild_public_prop:s0 exact string
ro.config.deprecated_cdma_supported u:object_r:radio_rild_public_prop:s0 exact string
ro.config.support_wcdma_modem1 u:object_r:radio_rild_public_prop:s0 exact string
msc.config.ismcoex u:object_r:radio_rild_public_prop:s0 exact string
ro.config.gsm_nonsupport u:object_r:radio_rild_public_prop:s0 exact string
msc.config.sglte u:object_r:radio_rild_public_prop:s0 exact string
ro.config.att.csg u:object_r:radio_rild_public_prop:s0 exact string
msc.config.extend_apdu u:object_r:radio_rild_public_prop:s0 exact string
msc.config.globalEcc u:object_r:radio_rild_public_prop:s0 exact string
msc.config.dsdspowerup u:object_r:radio_rild_public_prop:s0 exact string
ro.quick_broadcast_cardstatus u:object_r:radio_rild_public_prop:s0 exact string
ro.config.is_sup_cnap u:object_r:radio_rild_public_prop:s0 exact string
msc.config.updateCA_bycell u:object_r:radio_rild_public_prop:s0 exact string
ro.config.delay_attach_enabled u:object_r:radio_rild_public_prop:s0 exact string
msc.config.allow_pdp_auth u:object_r:radio_rild_public_prop:s0 exact string
persist.telephony.mpdn u:object_r:radio_rild_public_prop:s0 exact string
ro.config.wps_enabled u:object_r:radio_rild_public_prop:s0 exact string
ro.config.power u:object_r:radio_rild_public_prop:s0 exact string
ro.radio.vsim_dsds_version u:object_r:radio_rild_public_prop:s0 exact string
msc.config.clearcode_pdp u:object_r:radio_rild_public_prop:s0 exact string
msc.config.4.5gplus u:object_r:radio_rild_public_prop:s0 exact string
msc.config.laaplus u:object_r:radio_rild_public_prop:s0 exact string
msc.config.fake_ecc_list u:object_r:radio_rild_public_prop:s0 exact string
ro.config.ipv4.mtu u:object_r:radio_rild_public_prop:s0 exact string
ro.config.ipv6.mtu u:object_r:radio_rild_public_prop:s0 exact string
cdma.dormancy.mode u:object_r:radio_rild_public_prop:s0 exact string
cdma.dormancy.screen u:object_r:radio_rild_public_prop:s0 exact string
cdma.dormancy.delaytime u:object_r:radio_rild_public_prop:s0 exact string
cdma.dormancy.time_scroff u:object_r:radio_rild_public_prop:s0 exact string
gsm.fastdormancy.mode u:object_r:radio_rild_public_prop:s0 exact string
gsm.fastdormancy.delaytime u:object_r:radio_rild_public_prop:s0 exact string
gsm.fastdormancy.screen u:object_r:radio_rild_public_prop:s0 exact string
gsm.fastdormancy.time_scroff u:object_r:radio_rild_public_prop:s0 exact string
ro.odm.ca_product_version u:object_r:radio_rild_public_prop:s0 exact string
ro.odm.radio.nvcfg_normalization u:object_r:radio_rild_public_prop:s0 exact string
ro.product.imeisv u:object_r:radio_rild_public_prop:s0 exact string
ro.odm.disable_m1_gw_ps_attach u:object_r:radio_rild_public_prop:s0 exact string
msc.config.ecclist_nocard u:object_r:radio_rild_public_prop:s0 exact string
msc.config.ecclist_withcard u:object_r:radio_rild_public_prop:s0 exact string
ro.patchversion u:object_r:radio_rild_public_prop:s0 exact string
ro.comp.real.hl.product_base_version u:object_r:radio_rild_public_prop:s0 exact string
ro.comp.real.hl.product_cust_version u:object_r:radio_rild_public_prop:s0 exact string
ro.comp.real.hl.product_preload_version u:object_r:radio_rild_public_prop:s0 exact string

#Cust common property
msc.config.opta u:object_r:hw_cust_common_prop:s0 exact int
msc.config.optb u:object_r:hw_cust_common_prop:s0 exact int
msc.config.cota u:object_r:hw_cust_common_prop:s0 exact string
msc.sys.vendor u:object_r:hw_cust_common_prop:s0 exact string
msc.sys.country u:object_r:hw_cust_common_prop:s0 exact string
ro.product.locale.language u:object_r:hw_cust_common_prop:s0 exact string
ro.product.locale.region u:object_r:hw_cust_common_prop:s0 exact string
ro.product.CustCVersion u:object_r:hw_cust_common_prop:s0 exact string
ro.magic.cversion u:object_r:hw_cust_common_prop:s0 exact string
ro.product.CustDVersion u:object_r:hw_cust_common_prop:s0 exact string
msc.sys.oemName u:object_r:hw_cust_common_prop:s0 exact string
ro.build.version.magic u:object_r:hw_cust_common_prop:s0 exact string
ro.product.CotaVersion u:object_r:hw_cust_common_prop:s0 exact string

#audio public propertys
ro.config.ce_authenticate     u:object_r:honor_audio_public_read_prop:s0 exact string
ro.config.vol_steps           u:object_r:honor_audio_public_read_prop:s0 exact int
ro.config.music_region        u:object_r:honor_audio_public_read_prop:s0 exact string
ro.config.voice_cfg           u:object_r:honor_audio_public_read_prop:s0 exact string
ro.dts.licensepath            u:object_r:honor_dtsdecoder_public_read_prop:s0 exact string

# vendor-init-actionable|public-readable
persist.sys.msc.debug.on           u:object_r:dft_syshwdebug_prop:s0 exact int
ro.logsystem.usertype                 u:object_r:dft_logsystemtype_prop:s0 exact int

#whitelisted-public-props
ro.odm.config.higeo_fusion_ver     u:object_r:higeo_fusion_ver_public_read_prop:s0
ro.odm.config.higeo_map_matching   u:object_r:higeo_map_matching_public_read_prop:s0
ro.odm.config.higeo_nw_pos_db      u:object_r:higeo_nw_pos_db_public_read_prop:s0
ro.odm.config.higeo_pdrsupport     u:object_r:higeo_pdrsupport_public_read_prop:s0

#recovery
odm.androidboot.start_services         u:object_r:recovery_public_prop:s0
odm.recovery.start_all_srv             u:object_r:recovery_public_prop:s0
odm.factory.recovery.succssful_flg     u:object_r:recovery_public_prop:s0
odm.recovery.start_logcat              u:object_r:recovery_public_prop:s0
odm.recovery.start_logcat_ovs          u:object_r:recovery_public_prop:s0
ro.enter_erecovery                     u:object_r:enter_erecovery_prop:s0
ro.enter_erecovery_damaged_userdata    u:object_r:enter_erecovery_prop:s0

# wifi
ro.tether.denied            u:object_r:tether_denied_prop:s0
msc.config.abs_enable     u:object_r:config_hw_abs_enable_prop:s0
persist.sys.msc.wifiminidump.on        u:object_r:log_tag_prop:s0

# vendor-init-settable |  vendor-init-readable permissions
#facerecognize zengzhenyu 00416126  Begin
ro.odm.config.face_recognition         u:object_r:faceid_prop:s0 exact bool
#facerecognize zengzhenyu 00416126  End


#hwdebug
persist.log.remotedebug        u:object_r:log_tag_prop:s0
#for touch move
persist.msc.touch_vsync_opt u:object_r:honor_perf_persist_public_read_prop:s0
persist.msc.touch_move_opt u:object_r:honor_perf_persist_public_read_prop:s0
persist.msc.touchevent_opt u:object_r:honor_perf_persist_public_read_prop:s0

#for drm wifidisplay
odm.wfd_drm_enable u:object_r:wfd_drm_prop:s0  exact bool

#for bluetooth
ro.vendor.connectivity.sub_chiptype u:object_r:vendor_bt_chiptype_prop:s0

#replace the sensitive word
msc.config.   u:object_r:config_prop:s0
mscw.         u:object_r:system_prop:s0
msc.sys.      u:object_r:system_prop:s0

ro.support_ori_poweroffalarm u:object_r:honor_poweroffalarm_public_read_prop:s0 exact string
ro.support_wkalm_as_pwoffalm u:object_r:honor_wkalm_as_pwoffalm_public_set_prop:s0 exact string

# SR #7881 Oversea CloneApp l00013044
external_storage.cross_user.enabled u:object_r:external_storage_cross_user_enabled:s0

odm.recovery.start_svice                   u:object_r:recovery_public_prop:s0
odm.recovery.load_finish                   u:object_r:recovery_public_prop:s0

libc.debug.gwp.program u:object_r:libc_debug_prop:s0 exact string

#for DFT
oba.debug_on               u:object_r:oba_prop:s0

sys.hiview.first.boottime u:object_r:hiview_first_boot_time_prop:s0 exact string
# AR000FUMH5 m00022118 20220149
cache_key.get_pc_cast_mode               u:object_r:binder_cache_system_server_prop:s0
cache_key.get_virtual_display_id         u:object_r:binder_cache_system_server_prop:s0

# DTS2023052574712 w00013756 20230530
cache_key.static_user_props              u:object_r:binder_cache_system_server_prop:s0
# DTS2023090691588 w00013756 20230909
cache_key.user_properties                u:object_r:binder_cache_system_server_prop:s0

htee.version            u:object_r:htee_version_prop:s0
htee.basic            u:object_r:htee_basic_prop:s0

magic_agp_multiwindow_perf_optimize_enable  u:object_r:agp_multiwindow_perf_optimize_prop:s0

sf.anim.flag u:object_r:sf_anim_flag_prop:s0

#for secflash
secflash.state            u:object_r:secflash_state_prop:s0

odm.sys.start_fcme     u:object_r:fac_service_prop:s0
sys.rf_test.service    u:object_r:fac_service_prop:s0
sys.set_timeoffset     u:object_r:honor_set_timeoffset_prop:s0

#for log control pass when dex2oat
persist.log.control       u:object_r:logcontrol_prop:s0

#for trace service
persist.hiview.performance.traceservice u:object_r:system_prop:s0

#for antitheft
antitheft.init        u:object_r:antitheft_init_prop:s0

#for diagkit
persist.sys.diag.enhance        u:object_r:diagkit_enhance_prop:s0

#for tp_pen
tp.pen_supported       u:object_r:tp_pen_supported_prop:s0

#for satellite
sys.sat.smsc_address u:object_r:sys_sat_smsc_address_prop:s0
#line 1 "out/soong/.intermediates/system/sepolicy/contexts/system_ext_property_contexts/android_common/gen/newline"

#line 1 "vendor/honor/common/sepolicy/qcom/private/property_contexts"
#DTS2015110304872  machao/mwx306152 20151103 begin
persist.sys.jankdb              u:object_r:jankservice_prop:s0
#DTS2015110304872  machao/mwx306152 20151103 end

# BEGIN PN:DTS2015120407286, Added by huangdezhi/00176038, 20151209
# goldeneye.                         u:object_r:goldeneye_prop:s0
# END PN:DTS2015120407286, Added by huangdezhi/00176038, 20151209
#/*begin PN:DTS2015112705641 by z00183891 20151127*/
persist.config.hw.lca.region	u:object_r:logcat_prop:s0
#persist.sys.msc.debug.on	    u:object_r:logcat_prop:s0
#/*end PN:DTS2015112705641 by z00183891 20151127*/
#/*begin PN:DTS2015111601843 added by hxw281624 20151116*/
# persist.sys.smart_power            u:object_r:powergenie_prop:s0
# hw.flash.disabled.by.low_temp      u:object_r:powergenie_prop:s0

# for uniperf
sys.cpu.uniperf.ready    u:object_r:uniperf_ready_prop:s0

#for hwnff
# hwnff.server.start             u:object_r:log_tag_prop:s0
#sys.adb.authpass                           u:object_r:authpass_prop:s0
itouch.predict_opt    u:object_r:itouch_prop:s0
uifirst_listview_optimization_enable       u:object_r:uifirst_listview_prop:s0
systemui_fastview_fast_response_enable     u:object_r:systemui_fastview_prop:s0
settings_fastview_fast_response_enable     u:object_r:settings_fastview_prop:s0
launcher_smartslider_optimization_enable   u:object_r:launcher_smartslider_prop:s0
magic_perf_all_prop                    u:object_r:magic_perf_prop:s0
agp_prefetch_view_enable                   u:object_r:agp_prefetch_view_prop:s0
sys.itouch.config.trace_scenes             u:object_r:trace_input_scenes_prop:s0
msc.agp.screen_default_freq              u:object_r:hw_screen_default_freq_prop:s0
persist.agp.slide_model_parms              u:object_r:high_freq_slide_model_prop:s0
magic_agp_screen_brighten_animation       u:object_r:hwagp_screen_brighten_animation_prop:s0
magic_agp_graphic_gpu_tonemap             u:object_r:magic_agp_graphic_gpu_tonemap_prop:s0
magic_agp_graphic_gpu_precompile_shader   u:object_r:magic_agp_graphic_gpu_precompile_shader_prop:s0
magic_agp_charging_animation_3d_enable    u:object_r:hwagp_charging_animation_3d_prop:s0
magic_agp_window_blur_effect_enable       u:object_r:hwagp_window_blur_effect_prop:s0
magic_agp_window_blur_sched_enable        u:object_r:hwagp_window_blur_sched_prop:s0
sf.hwui.viewBlurBackground                u:object_r:hwagp_view_blur_background_prop:s0
magic_graphic_features_switch               u:object_r:hwagp_graphic_features_switch:s0
persist.sys.standard_freq_slide_enable     u:object_r:standard_freq_slide_model_prop:s0

ro.vendor.tui.service                      u:object_r:tee_tui_prop:s0

# each prop in whitelist should belong to one owner, and list the owner in comment
# vendor-init-readable


# vendor-init-settable


# vendor-init-readable|vendor-init-actionable


# vendor-init-settable|vendor-init-actionable


# public-readable



# vendor-init-settable|public-readable
ro.confg.hw_sbl1version u:object_r:vendor_default_prop:s0 exact string
ro.confg.hw_appsbootversion u:object_r:vendor_default_prop:s0 exact string
# vendor-init-actionable|public-readable

#for erecovery wifi property whitelist
odm.erecovery.run.wcnss                u:object_r:erecovery_wifi_prop:s0
odm.erecovery.start.modem              u:object_r:erecovery_modem_prop:s0

persist.radio.enable_check_slot2_sim_status  u:object_r:slot2_sim_status_prop:s0
bms.logcat.enabled         u:object_r:bms_logcat_prop:s0

# for DFT
ro.product.enable.logcontrol    u:object_r:logcontrol_product_prop:s0

# for coredump
persist.debug.trace  u:object_r:coredump_debug_prop:s0
persist.debug.coredump_processes  u:object_r:coredump_debug_prop:s0
persist.debug.coredump_comm_pid u:object_r:coredump_debug_prop:s0

# for bluetooth leaudio
ro.bluetooth.leaudio_offload.supported     u:object_r:leaudio_offload_supported_prop:s0
ro.bluetooth.leaudio_switcher.supported    u:object_r:leaudio_switcher_supported_prop:s0

#line 1 "out/soong/.intermediates/system/sepolicy/contexts/system_ext_property_contexts/android_common/gen/newline"

